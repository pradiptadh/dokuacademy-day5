package task4;

public class Omnivor extends Animals{
    public String teeth2;
    public Omnivor(String name, String type_food, String teeth , String teeth2) {
        super(name, type_food, teeth);
        this.teeth2 = teeth2;
    }

    public static void identify_myself(String name , String type_food, String teeth , String teeth2) {
        System.out.println("Hi I'm Omnivor , My Name is " + name + ",  My Food is " + type_food + ", I have " + teeth + " And " + teeth2 + " teeth");
    }
}
