package task4;

public class Herbivor extends Animals{
    public Herbivor(String name, String type_food, String teeth) {
        super(name, type_food, teeth);
    }

    public static void identify_myself(String name , String type_food, String teeth) {
        System.out.println("Hi I'm Herbivor , My Name is " + name + ",  My Food is " + type_food + ", I have " + teeth + "teeth");
    }
}
