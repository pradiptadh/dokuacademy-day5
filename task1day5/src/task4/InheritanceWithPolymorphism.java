package task4;

public class InheritanceWithPolymorphism {
    public void execute(){
        Animals animals = new Animals("Binatang","Kucing" ,"21");
        System.out.println("Hi I'm Parent of All Animals , My name is " + animals.name);
        System.out.println();
        Herbivor.identify_myself("Kambing","tumbuhan"," tumpul");
        Carnivor.identify_myself("Singa","daging"," tajam");
        System.out.println();
        Omnivor.identify_myself("Ayam","semua","tajam" , "tumpul");

    }
}
