package task4;

public class Animals {
    public String name;
    public String type_food;
    public String teeth;

    public Animals(String name, String type_food, String teeth) {
        this.name = name;
        this.type_food = type_food;
        this.teeth = teeth;
    }
}
