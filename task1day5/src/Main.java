import task1.Encapsulation;
import task2.*;
import task3.InheritanceWithPolymorphism;
import task5.GuessTheCage;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        int choice;
        Scanner input = new Scanner(System.in);
        System.out.println("==========================");
        System.out.println("1 . Task 1 Encapsulation ");
        System.out.println("2 . Task 2 Abstraction");
        System.out.println("3 . Task 3 Inheritance & Polymorphism (Vehicle)");
        System.out.println("4 . Task 4 Inheritance & Polymorphism (Animal)");
        System.out.println("5 . Task 5 Guess The Cage");
        System.out.println("===========================");
        System.out.println("Pilih task yang ingin ditampilkan : ");
        choice = input.nextInt();

        switch (choice){
            case 1 :
                Encapsulation encapsulation = new Encapsulation();
                encapsulation.execute();
                break;
            case 2 :
                Abstraction abstraction = new Abstraction();
                abstraction.execute();
                break;
            case 3 :
                InheritanceWithPolymorphism iwp = new InheritanceWithPolymorphism();
                iwp.execute();
                break;
            case 4 :
                task4.InheritanceWithPolymorphism iwp4 = new task4.InheritanceWithPolymorphism();
                iwp4.execute();
                break;
            case 5 :
                GuessTheCage gtc = new GuessTheCage();
                gtc.execute();
                break;

        }

    }
}