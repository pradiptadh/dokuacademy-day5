package task2;

import java.util.Scanner;

public class Abstraction {
    public static void execute(){
        Scanner input = new Scanner(System.in);
        Calculator calc = Calculator.getInstance();
        int value1 = 0 , value2=0;
        System.out.println("+++++++++++Calculator++++++++++++");
        System.out.println("1. Open Calculator");
        System.out.println("99. Exit");
        System.out.println("Masukkan Pilihan Anda : ");
        int choice = input.nextInt();
        switch (choice){
            case 1 :
                System.out.println("+++++++++++Calculator++++++++++++");
                System.out.println("Masukkan Value1 = ");
                value1 = input.nextInt();
                System.out.println("Masukkan Value2 = ");
                value2 = input.nextInt();
                System.out.println("+++++++++++Calculator++++++++++++");
                System.out.println("Please Enter Calculation Operation :");
                System.out.println("1. Add Value");
                System.out.println("2. Sub Value");
                System.out.println("3. Multiply Value");
                System.out.println("4. Divide Value");
                System.out.println("+++++++++++Calculator++++++++++++");
                System.out.println("Pilihan anda ");
                int pilihan = input.nextInt();
                switch (pilihan){
                    case 1 :
                        calc.setCalculationStrats(new AddOperation());
                        System.out.println(calc.calculate(value1,value2));
                        break;
                    case 2 :
                        calc.setCalculationStrats(new SubstractOperation());
                        System.out.println(calc.calculate(value1 ,value2));
                        break;
                    case 3 :
                        calc.setCalculationStrats(new MutiplyOperation());
                        System.out.println(calc.calculate(value1 ,value2));
                        break;
                    case 4 :
                        calc.setCalculationStrats(new DivideOperation());
                        System.out.println(calc.calculate(value1 ,value2));
                        break;
                    default:
                        System.out.println("Inputan yang anda masukkan salah!");
                }
                break;
            case 99:
                System.out.println("Exit");
                break;
            default:
                System.out.println("Inputan yang anda masukan salah!");
                break;
        }
    }
}
