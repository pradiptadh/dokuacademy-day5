package task2;

public class SubstractOperation implements CalculationStrats{
    @Override
    public float calculate(float value1, float value2) {
        return value1 - value2;
    }
}
