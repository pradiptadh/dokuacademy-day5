package task2;

public class Calculator {
    public static Calculator instance = null;

    CalculationStrats calculationStrats;

    public void setCalculationStrats(CalculationStrats calculationStrats) {
        this.calculationStrats = calculationStrats;
    }

    public static Calculator getInstance(){
        if(instance == null){
            instance = new Calculator();
        }
        return instance;
    }

    public float calculate(float value1, float value2) {
        return calculationStrats.calculate(value1, value2);
    }
}
