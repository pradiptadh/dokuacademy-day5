package task2;

public class AddOperation implements CalculationStrats{
    @Override
    public float calculate(float value1, float value2) {
        return value1 + value2;
    }
}
