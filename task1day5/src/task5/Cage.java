package task5;

public class Cage implements GuessCage{
    public String animal;
    public Integer number, guess;

    public Cage(String animal, Integer number, Integer guess) {
        this.animal = animal;
        this.number = number;
        this.guess = guess;
    }

    public Cage(Integer number){
        this.number = number;
    }

    public Cage(String animal , Integer guess){
        this.animal = animal;
        this.guess = guess;
    }

    public void setAnimal(String animal){
        this.animal = animal;
    }
    public void setGuess(Integer guess){
        this.guess = guess;
    }

    @Override
    public void open_cage(String animal) {
    }

    @Override
    public void create_cage(Integer number) {
    }
}
