package task5;

import java.util.*;

public class GuessTheCage {
    public void execute(){
        final String NORMAL = "\u001b[0m";
        final String BLUE = "\u001b[34m";
        final String RED = "\u001b[31m";
        final String YELLOW = "\u001b[33m";
        final String GREEN = "\u001b[32m";
        int choice, guess , number;
        Integer guessTrue = 0 ;

        String animal;

        Scanner input = new Scanner(System.in);


        //show menu
        Board.menu();
        choice = input.nextInt();
        switch (choice){
            case 1 :
                System.out.print("Masukkan Jumlah Kandang : ");
                number = input.nextInt();
                Board board = new Board(number);
                for (int i = 1; i <=number; i++) {
                    board.create_cage(i);
                }

                System.out.print("Pilih Kandang Yang Ingin Dibuka : ");
                guess = input.nextInt();

                System.out.println( NORMAL +"Masukkan tebakkan: ");
                animal = input.nextLine().toUpperCase();
                Board board2 = new Board(animal,number,guess);

                Random random = new Random();
                String[] animals = {"B", "Z", "K"};
                String randomAnimals = animals[random.nextInt(animals.length)];

                List<String> listWords = new ArrayList<>();
                for (int i = 0; i<number; i++) {
                    listWords.add(randomAnimals);
                }

                List<String> list = listWords;
                Map<Integer, String> map = new HashMap<>();
                for (int i = 0; i<list.size(); i++) {
                    map.put(i, list.get(i));
                }

                Map<Integer, String> listAnimals = map;
                System.out.println(listAnimals);

                Map<Integer, String> listTrue = new HashMap<>();

                for (int i = 0; i < listAnimals.size(); i++) {

                    if (listAnimals.get(guess-1).equals(animal)){
                            listTrue.put(guess,animal);
                            
                    }
                    if (i == listAnimals.size()) {
                            System.out.println(GREEN +"Congrats your are Win!" + NORMAL);
                            break;
                    }

                    System.out.println("---PILIHAN---");
                    System.out.println(BLUE +"K" + NORMAL +": Kambing");
                    System.out.println(RED +"Z"+NORMAL+ ": Zebra");
                    System.out.println(YELLOW +"B"+ NORMAL+ ": Bebek");
                    System.out.println( NORMAL +"Masukkan tebakkan: ");
                    animal = input.nextLine().toUpperCase();
                    board2.setAnimal(animal);


                    System.out.print("Pilih Kandang Yang Ingin Dibuka : ");
                    guess = input.nextInt();
                    board2.setGuess(guess);

                }
                for (Integer value: listTrue.keySet()) {
                    board2.open_cage(listAnimals.get(value-1));
                }


                break;
            case 99 :
                System.out.println("Exit");
                break;
            default:
                System.out.println("Maaf Format Inputan Anda Salah!");
                break;

        }

    }
}
