package task5;


public class Board extends Cage{
    public Board(String animal, Integer number, Integer guess) {
        super(animal, number, guess);
    }

    public Board(Integer number) {
        super(number);
    }

    public Board(String animal, Integer guess) {
        super(animal, guess);
    }

    public static void menu(){
        System.out.println("-----------------------");
        System.out.println("     Tebak Kandang    ");
        System.out.println("-----------------------");
        System.out.println("1. Jumlah Kandang");
        System.out.println("99. Exit");
        System.out.println("Pilih menu : ");
    }

    @Override
    public void open_cage(String animal) {
        super.open_cage(animal);
        System.out.println("|||");
        System.out.println("|"+ animal +"|");
        System.out.println("|||");
        System.out.println();

    }

    @Override
    public void create_cage(Integer number) {
        super.create_cage(number);
        System.out.println("|||");
        System.out.println("|"+ number +"|");
        System.out.println("|||");
        System.out.println();
    }


}
