package task1;

public class Fish {
    public String type ;
    public String feed;

    public Fish(String type, String feed) {
        this.type = type;
        this.feed = feed;
    }

    public void show_identity(String type, String feed){
        System.out.println("Saya Ikan dengan detail , Jenis : "+ type + ", Makanan : " + feed);
    }
}
