package task1;

public class Flower {
    public String name;
    public String color;
    public Integer num_of_petal;

    public Flower(String name, String color, Integer num_of_petal) {
        this.name = name;
        this.color = color;
        this.num_of_petal = num_of_petal;
    }

    public void show_identity(String name, String color , String num_of_petal){
        System.out.println("Saya Bunga dengan detail , Jenis : "+ name + ", color : " + color + "  , num of petal : " + num_of_petal );
    }
}
