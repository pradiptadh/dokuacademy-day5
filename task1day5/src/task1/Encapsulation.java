package task1;

import java.util.ArrayList;

public class Encapsulation {
    public void execute(){
        ArrayList<Cat> arrCat = new ArrayList<>();
        arrCat.add(new Cat("Hitam" , 4));
        arrCat.add(new Cat("Putih" , 3));
        arrCat.add(new Cat("Biru" , 4));
        arrCat.add(new Cat("Merah" , 3));
        arrCat.add(new Cat("Jingga" , 4));

        for (Cat cat : arrCat) {
            cat.show_identity(cat.fur_color, cat.num_of_leg);
        }

        System.out.println();

        ArrayList<Fish> arrFish = new ArrayList<>();
        arrFish.add(new Fish("paus", "plankton"));
        arrFish.add(new Fish("cupang", "cacing"));
        arrFish.add(new Fish("arwarna", "jangkrik"));
        arrFish.add(new Fish("sapu-sapu", "pelet"));

        for (Fish fish : arrFish) {
            fish.show_identity(fish.type, fish.feed);
        }

        System.out.println();

        ArrayList<Flower> arrFlowers = new ArrayList<>();
        arrFlowers.add(new Flower("Bangkai","merah" , 12));
        arrFlowers.add(new Flower("Anggrek","putih" , 8));
        arrFlowers.add(new Flower("Mawar","merah" , 3));
        arrFlowers.add(new Flower("Melati","kuning" , 5));

        for (Flower flowers: arrFlowers) {
            flowers.show_identity(flowers.name,flowers.color,flowers.num_of_petal.toString());
        }

        System.out.println();

        ArrayList<Car> arrCars = new ArrayList<>();
        arrCars.add(new Car("sedan","merah",4));
        arrCars.add(new Car("truk","hijau",6));
        arrCars.add(new Car("tronton","coklat",12));
        arrCars.add(new Car("angkot","kuning",4));

        for (Car cars : arrCars) {
            cars.show_identity(cars.type, cars.color, cars.num_of_tired.toString());
        }

    }

}
