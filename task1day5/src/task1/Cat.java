package task1;

import java.util.ArrayList;

public class Cat {
    public String fur_color ;
    public Integer num_of_leg;

    public Cat(String fur_color, Integer num_of_leg) {
        this.fur_color = fur_color;
        this.num_of_leg = num_of_leg;
    }

    public void show_identity(String fur_color, Integer num_of_leg){
        System.out.println("Saya Kucing dengan detail , Warna Bulu : "+ fur_color + ", dengan jumlah kaki : " + num_of_leg);
    }

}
