package task1;

public class Car {
    public String type;
    public String color;
    public Integer num_of_tired;

    public Car(String type, String color, Integer num_of_tired) {
        this.type = type;
        this.color = color;
        this.num_of_tired = num_of_tired;
    }

    public void show_identity(String type, String color , String num_of_tired){
        System.out.println("Saya Mobil dengan detail , Type : "+ type + ", color : " + color + "  , num of tired : " + num_of_tired );
    }
}
