package task3;

public class Cars extends Vehicles{
    public Integer wheel_count;
    public String engine_type;
    public Cars(String name, String with_engine , Integer wheel_count , String engine_type) {
        super(name, with_engine);
        this.engine_type = engine_type;
        this.wheel_count = wheel_count;
    }


    public void identify_myself(String name, String with_engine, Integer wheel_count, String engine_type ){
        System.out.println("Hi I'm Car , My Name is " + name + ", My Engine Status is " + with_engine + ", I have " + wheel_count + " Wheel(s)" + ", My Engine Type Is " + engine_type);
    }

}
