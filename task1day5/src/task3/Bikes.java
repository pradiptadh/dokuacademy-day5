package task3;

public class Bikes extends Vehicles{
    public Integer wheel_count;
    public Bikes(String name, String with_engine, Integer wheel_count) {
        super(name, with_engine);
        this.wheel_count = wheel_count;
    }
    public void identify_myself(String name , String with_engine , Integer wheel_count ){
        System.out.println("Hi I'm Bike, My Name is " + name + ", My Engine Status is " + with_engine + ", I have " + wheel_count + " Wheel(s)");
    }
}
