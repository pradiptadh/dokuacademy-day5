package task3;

import java.util.ArrayList;

public class InheritanceWithPolymorphism {
    public void execute(){
        Vehicles vehicles = new Vehicles("Gerobak" , "no_engine");
        System.out.println("Hi I'm Parent of all Vehicles , My Name is " + vehicles.name + ", My Engine Status is " + vehicles.with_engine);

        System.out.println();

        ArrayList<Bikes> arrBikes = new ArrayList<>();
        arrBikes.add(new Bikes("Pedal Bikes","no_engine" , 2));
        arrBikes.add(new Bikes("Motor Bikes","with_engine" , 2));

        for (Bikes bikes: arrBikes) {
            bikes.identify_myself(bikes.name,bikes.with_engine, bikes.wheel_count);
        }

        System.out.println();

        ArrayList<Cars> arrCars = new ArrayList<>();
        arrCars.add(new Cars("Sport Cars" ,"with_engine" , 4 , "V8"));
        arrCars.add(new Cars("Pickup Cars" ,"with_engine" , 4 , "Solar"));

        for (Cars cars : arrCars) {
            cars.identify_myself(cars.name,cars.with_engine, cars.wheel_count , cars.engine_type);
        }

        System.out.println();

        ArrayList<Buses> arrBuses = new ArrayList<>();
        arrBuses.add(new Buses("Public Bus", "Trans Jakarta", "with_engine" , 4));
        arrBuses.add(new Buses("Private Bus", "School Bus", "with_engine" , 4));

        for (Buses buses : arrBuses) {
            buses.identify_myself(buses.private_bus,buses.name, buses.with_engine , buses.wheel_count);
        }


    }
}
