package task3;

public class Buses extends Vehicles{

    public Integer wheel_count;
    public String private_bus;
    public Buses(String private_bus , String name, String with_engine , Integer wheel_count ) {
        super(name, with_engine );
        this.private_bus = private_bus;
        this.wheel_count = wheel_count;
    }


    public void identify_myself(String private_bus , String name , String with_engine , Integer wheel_count){
        System.out.println("Hi I'm Bus "+ private_bus + ", My Name is " + name + ", My Engine Status is " + with_engine + ", I have " + wheel_count + " Wheel(s)");
    }
}
